import termcolor
import pandas as pd

iteration = 0
iterations = 502
experiment_data = {'time': [], 'voltage': []}

while iteration <= iterations:
    experiment_data['time'].append(iteration * 0.01)
    experiment_data['voltage'].append(12)

    iteration += 1

print(termcolor.colored('Experiment successfully finished!', color='green'))
out_data = pd.DataFrame(data=experiment_data)

out_data.to_csv('voltage.csv')
