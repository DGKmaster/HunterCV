import termcolor
import pandas as pd

iteration = 0
iterations = 501
experiment_data = {'aaa': [], 'speed': []}
data = pd.DataFrame()
data = data.from_csv(f'experiment_1.csv')

while iteration <= iterations:
    experiment_data['aaa'].append(data['time'][iteration])
    experiment_data['speed'].append(data['speed'][iteration])
    print(experiment_data['speed'][iteration])
    iteration += 1

print(termcolor.colored('Experiment successfully finished!', color='green'))
out_data = pd.DataFrame(data=experiment_data)

out_data.to_csv('speed_1.csv')