import matplotlib.pyplot as plt
import pandas as pd


for i in range(1, 11):
    data = pd.DataFrame()
    data = data.from_csv(f'experiment_{i}.csv')

    print(data)
    fig, ax = plt.subplots()
    ax.plot(data['time'], data['speed'])
    ax.set_title('Speed')

    plt.savefig(f'plot_{i}.png')
