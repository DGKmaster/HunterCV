import serial
import time
import pandas as pd
import termcolor

flag = True

while flag:
    try:
        ser = serial.Serial('COM5', 250000, timeout=0)
        flag = False
    except:
        print("Can't connect to target device... Try again")

time0 = time.time()
time1 = 0
tick0 = 0
ticks = 0
dr = 0.01
iteration = 0
iterations = 500

experiment_data = {'time': [], 'speed': []}

while iteration <= iterations:
    data = ser.readline()
    try:
        ticks = int(data.decode("utf-8"))

        # print(f"Position={round(ticks / 3000)} \nTime={time1}")
        time1 = time.time() - time0

        if time1 > dr:
            with open('parameters.json') as parameters_file:
                if (ticks - tick0) < 0:
                    experiment_data['time'].append(iteration * dr)
                    experiment_data['speed'].append(None)
                    continue

                speed = round((ticks - tick0) * 60 / (3000 * time1), 2)
                experiment_data['time'].append(iteration * dr)
                experiment_data['speed'].append(speed)
                print(f"Iteration: {iteration} \nSpeed = {speed} rpm \n-----------------------\n")
                iteration += 1
                time0 = time.time()
                time1 = 0
                tick0 = ticks

    except:
        pass

print(termcolor.colored('Experiment successfully finished!', color='green'))
out_data = pd.DataFrame(data=experiment_data)

out_data.to_csv('experiment_3.csv')


    #except:
    #    print('Data could not be read')
    #    time.sleep(1)