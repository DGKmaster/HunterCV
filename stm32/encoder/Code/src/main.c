#include <stm32f4xx.h>

int main() {
    /* Разрешаем работу с портом D */
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
    /* Настройка пинов PD12-15 */
    GPIOD->MODER |= GPIO_MODER_MODER12;
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR12;
    GPIOD->PUPDR |= GPIO_PUPDR_PUPDR12;

    GPIOD->MODER |= GPIO_MODER_MODER13;
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR13;
    GPIOD->PUPDR |= GPIO_PUPDR_PUPDR13;

    GPIOD->MODER |= GPIO_MODER_MODER14;
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR14;
    GPIOD->PUPDR |= GPIO_PUPDR_PUPDR14;

    GPIOD->MODER |= GPIO_MODER_MODER15;
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15;
    GPIOD->PUPDR |= GPIO_PUPDR_PUPDR15;
    
/* Leds: PD12-15 */
/* main loop */    
	while(1) {
        GPIOD->BSRRH |= GPIO_BSRR_BS_12;
        GPIOD->BSRRH |= GPIO_BSRR_BR_13;
        GPIOD->BSRRH |= GPIO_BSRR_BS_14;
        GPIOD->BSRRH |= GPIO_BSRR_BR_15;
        delay(1000);
        GPIOD->BSRRH |= GPIO_BSRR_BR_12;
        GPIOD->BSRRH |= GPIO_BSRR_BS_13;
        GPIOD->BSRRH |= GPIO_BSRR_BR_14;
        GPIOD->BSRRH |= GPIO_BSRR_BS_15;
        delay(1000);
    }
}

void delay(uint32_t time) {
    for (uint32_t i; i <= time; i++) {

    };
}

/* read signal from incremental encoder */
int readEncoder() {
    return 0;
}

/* send msg to PC */
int sendUART() {
    return 0;
}

/* configurate quad-timers, UART etc. */
void config() {
    
}
