#pragma once

#include <Base.hpp>

/// Interface for
class Track
{
protected:
    Base *_core;
    cv::Rect _track_bbox;

public:
    /// Default constructor
    Track();

    /// Default destructor
    ~Track();
};