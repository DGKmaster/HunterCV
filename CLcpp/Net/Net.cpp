#include "Net.hpp"

/// Default constructor
Net::Net()
{
    _network_width = 416;
    _network_height = 416;
    _probability_index = 5;
}

Net::~Net()
{
}

int Net::setParams()
{
    _net_cfg_path = "/home/dgk/Documents/HunterCV/CLcpp/yolo/cfg/yolo.cfg";
    _net_weights_path = "/home/dgk/Documents/HunterCV/CLcpp/yolo/model/yolo.weights";
    _net_min_confidence = 0.02;
    _net_class_names_path = "/home/dgk/Documents/HunterCV/pyway/cfg/coco.names";

    std::ifstream _net_class_names_file(_net_class_names_path.c_str());
	if(_net_class_names_file.is_open())
	{
	    std::string class_name = "";
	    while(_net_class_names_file >> class_name)
		{
		    _net_class_names_vec.push_back(class_name);
		}
	}
    return 0;
}

/// Initialize network
int Net::readNet()
{
    _net = cv::dnn::readNetFromDarknet(_net_cfg_path, _net_weights_path);
    if(_net.empty())
	{
	    std::cerr << "Can't load network by using the following files: " << std::endl;
	    std::cerr << "cfg-file:     " << _net_cfg_path << std::endl;
	    std::cerr << "weights-file: " << _net_weights_path << std::endl;
	    std::cerr << "Models can be downloaded here:" << std::endl;
	    std::cerr << "https://pjreddie.com/darknet/yolo/" << std::endl;
	    exit(-1);
	}

    return 0;
}

/// Set Base for read frame and
/// net parameters
int Net::setBase(Base *base)
{
    _core = base;

    return 0;
}

/// Detect all objects in video frame
int Net::detectObj()
{
    cv::Mat frame;

    frame = _core->copyFrame();
    cv::namedWindow("Input", cv::WINDOW_AUTOSIZE);
    cv::namedWindow("Resized", cv::WINDOW_NORMAL);

    cv::imshow("Input", frame);

    cv::resize(frame, _resized, cv::Size(_network_width, _network_height));
    cv::imshow("Resized", _resized);

    /// Prepare blob
    cv::Mat inputBlob = cv::dnn::blobFromImage(_resized, 1 / 255.F); // Convert Mat to batch of images
    _net.setInput(inputBlob, "data");                                // set the network input
                                                                     /// Make forward pass
    _detectionMat = _net.forward("detection_out");                   // compute output
    for(int i = 0; i < _detectionMat.rows; i++)
	{

	    const int probability_size = _detectionMat.cols - _probability_index;
	    float *prob_array_ptr = &_detectionMat.at<float>(i, _probability_index);

	    size_t objectClass = std::max_element(prob_array_ptr, prob_array_ptr + probability_size) - prob_array_ptr;
	    float confidence = _detectionMat.at<float>(i, (int)objectClass + _probability_index);

	    if(confidence > _net_min_confidence)
		{
		    float x = _detectionMat.at<float>(i, 0);
		    float y = _detectionMat.at<float>(i, 1);
		    float width = _detectionMat.at<float>(i, 2);
		    float height = _detectionMat.at<float>(i, 3);
		    int xLeftBottom = static_cast<int>((x - width / 2) * _core->getFrame().cols);
		    int yLeftBottom = static_cast<int>((y - height / 2) * _core->getFrame().rows);
		    int xRightTop = static_cast<int>((x + width / 2) * _core->getFrame().cols);
		    int yRightTop = static_cast<int>((y + height / 2) * _core->getFrame().rows);

		    cv::Rect object(xLeftBottom, yLeftBottom, xRightTop - xLeftBottom, yRightTop - yLeftBottom);

		    _core->drawRect(object);
		    std::cout << "Class: " << objectClass << std::endl; // TODO:
		    std::cout << "Confidence: " << confidence << std::endl;
		    std::cout << " " << xLeftBottom << " " << yLeftBottom << " " << xRightTop << " " << yRightTop
		              << std::endl;

		    if(objectClass < _net_class_names_vec.size())
			{
			    _ss.str("");
			    _ss << confidence;
			    cv::String conf(_ss.str());
			    cv::String label = cv::String(_net_class_names_vec[objectClass]) + ": " + conf;
			    int baseLine = 0;
			    cv::Size labelSize = cv::getTextSize(label, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
			    cv::rectangle(frame, cv::Rect(cv::Point(xLeftBottom, yLeftBottom - labelSize.height),
			                             cv::Size(labelSize.width, labelSize.height + baseLine)),
			        cv::Scalar(255, 255, 255), CV_FILLED);
			    cv::putText(frame, label, cv::Point(xLeftBottom, yLeftBottom), cv::FONT_HERSHEY_SIMPLEX,
			        0.5, cv::Scalar(0, 0, 0));
			}
		    else
			{
			}
		}
	}

    return 0;
}