#pragma once
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include <opencv2/dnn.hpp>
#include <opencv2/dnn/shape_utils.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <Base.hpp>

/// Class for loading Neural Network and predict class
class Net
{
protected:
    /// TODO: Find what it is
    size_t _network_width;
    /// TODO: Find what it is
    size_t _network_height;

    /// Net parameters
    cv::String _net_cfg_path;
    cv::String _net_weights_path;
    float _net_min_confidence;
    cv::String _net_class_names_path;

    std::vector<std::string> _net_class_names_vec;
	
	/// Create formatted string
	std::ostringstream _ss;
	
    cv::dnn::Net _net;

    Base *_core;

    cv::Mat _resized;

    cv::Rect _net_bbox;

    cv::Mat _detectionMat;

    int _probability_index;

public:
    Net();
    ~Net();

    /// Initialize network
    int readNet();

    /// Set Net cfg and weights
    int setParams();

    /// Prepare image
    int imgPrepare();

    /// Detect all objects in video frame
    int detectObj();

    /// Set Base for read frame and
    /// net parameters
    int setBase(Base *base);
};
