
#include <Base.hpp>
#include <Hunter.hpp>
#include <Net.hpp>
#include <Track.hpp>

int main(int argc, char **argv)
{
    /// Camera initialisation and
    /// parameters parsing
    Base base;
    base.initParams(argc, argv);
    base.initCamera();

    Net net;
    net.setBase(&base);
    net.setParams();

    int k;
    while(true)
	{
	    base.readFrame();
	    base.showOrSaveImg();
	    k = cv::waitKey(1) & 0xff;
	    if(k == 27)
		{
		    break;
		}
	}

    while(true)
	{
	    base.readFrame();
	    base.showOrSaveImg();
	    // base.imgPrepare();
	    net.detectObj();
	    cv::waitKey(30);
	    base.showOrSaveImg();
	}
    return 0;
}