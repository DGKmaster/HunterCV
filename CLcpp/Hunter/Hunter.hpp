#pragma once

#include <Base.hpp>
#include <Net.hpp>
#include <Track.hpp>

class Hunter
{
protected:
	Net *_net;
	Track *_track;
	cv::Rect *_net_bbox;
	cv::Rect *_track_bbox;
 	
public:
    /// Default constructor
	Hunter();
	/// Default destructor
    ~Hunter();
	
	/// Read frame, give it to Track and Net
	/// and verify result
	int work();
	
	/// Draw track line and bbox from Track
	/// and Net and show debug info
	int workAndShow();
	
	/// Set net pointer
	int setNet(Net &net);
	/// Set track pointer
	int setTrack(Track &track);
};
