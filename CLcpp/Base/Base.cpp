#include "Base.hpp"

/// Default constructor
Base::Base()
{
}

/// Default destructor
Base::~Base()
{
}

/// Parse command line and set up all parameters
int Base::initParams()
{
    _camera_device = 0;

    return 0;
}

/// Draw rectangle
int Base::drawRect(cv::Rect rectangle)
{
    cv::rectangle(_frame, rectangle, cv::Scalar(0, 255, 0));

    return 0;
}

/// Calculate FPS
/*int calcFPS()
{
    _freq = cv::getTickFrequency() / 1000;
    //double time = _net.getPerfProfile(layersTimings) / freq;
    _ss << "FPS: " << 1000 / _time << " ; time: " << _time << " ms";
        putText(_frame, ss.str(), cv::Point(20, 20), 0, 0.5, cv::Scalar(0, 0, 255));
}*/

/// Initialise camera
int Base::initCamera()
{

    _cap = cv::VideoCapture(_camera_device);
    if(!_cap.isOpened())
	{
	    std::cout << "Couldn't find camera: " << _camera_device << std::endl;
	    return -1;
	}

    return 0;
}

/// Read frame
int Base::readFrame()
{
    _cap >> _frame; // get a new frame from camera/video or read image
    if(_frame.channels() == 4)
	{
	    cvtColor(_frame, _frame, cv::COLOR_BGRA2BGR);
	}

    return 0;
}

/// Show image
int Base::showOrSaveImg()
{
    cv::imshow("Detections", _frame);
    return 0;
}

/// Get video frame
cv::Mat Base::getFrame()
{
    return _frame;
}

/// Copy video frame
cv::Mat Base::copyFrame()
{
    cv::Mat output_frame = _frame.clone();

    return output_frame;
}