#pragma once

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iostream>

#include <opencv2/dnn.hpp>
#include <opencv2/dnn/shape_utils.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

/// Class for dealing with video and GUI
class Base
{
protected:
    /// Object for storing camera data
    cv::VideoCapture _cap;
    /// Current video frame
    cv::Mat _frame;

    /// Number of camera device
    int _camera_device;

    /// To calculate FPS
    std::vector<double> _layersTimings;
    double _freq;
    double _time;

    std::ostringstream _ss;

public:
    /// Default constructor
    Base();

    /// Default destructor
    ~Base();

    /// Initialise camera
    int initCamera();

    /// Parse command line and set up all parameters
    int initParams();

    /// Read video frame
    int readFrame();

    /// Get video frame
    cv::Mat getFrame();

    /// Copy video frame
    cv::Mat copyFrame();
	
    /// Get pointer to video frame
    cv::Mat *getPointerFrame();

    /// Prepare image
    cv::Mat imgPrepare();

    /// Calculate FPS
    // int calcFPS();

    /// Show image
    int showOrSaveImg();

    /// Draw rectangle
    int drawRect(cv::Rect rectangle);

    /// Get Net configuration path from parser
    cv::String getNetCfgPath();

    /// Get Net weigths path from parser
    cv::String getNetWeightsPath();

    /// Get Net min confidence for detection
    float getNetMinConfidence();
};
