import time

import numpy as np
from darkflow.net.build import TFNet


class Net:
    # Set YOLO options
    def setup(self):
        options = {"model": "cfg/yolo.cfg", "load": "model/yolo.weights", "threshold": 0.1}
        self.tf_net_options = TFNet(options)

    # Predict object class and return bbox in cup case
    def detect(self, frame):
        frame = np.asarray(frame)

        start_time = time.time()
        result = self.tf_net_options.return_predict(frame)
        end_time = time.time()
        result_time = end_time - start_time
        print('Detected at' + str(result_time) + ' s')

        for x in result:
            if x['label'].find('person') > -1:
                x0 = int(x['topleft']['x'])
                y0 = int(x['topleft']['y'])
                x1 = int(x['bottomright']['x'])
                y1 = int(x['bottomright']['y'])

                width = x1 - x0
                height = y1 - y0

                self.bbox = (x0, y0, width, height)
                print('Person is found')

                return 0

        print('Person is NOT found')
        return 1
