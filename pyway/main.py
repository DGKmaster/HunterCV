import sys

import hunter
import parser

# python3 -m cProfile -o profile.pstats main.py -v 320 --image show --calibration on --bbox coord --debug gui --net on -s cam
# python3 -m pstats profile.pstats

# Parse input arguments
arguments_parser = parser.create()
arguments_list = arguments_parser.parse_args(sys.argv[1:])

# Init base flags and params 
u_hunter = hunter.Hunter(arguments_list.net)

# Load all parameters, apply default constructors
u_hunter.setup(arguments_list.stream, arguments_list.version, arguments_list.parameter)

if arguments_list.calibration == 'on':
    # Calibrate camera if chosen
    u_hunter.calibrate()

# Set tracked object
u_hunter.set_obj(arguments_list.bbox)

# Update bbox and net check
flag = True
while flag:
    flag = u_hunter.update()
