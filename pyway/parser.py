import argparse


# Parser for input arguments
def create():
    parser = argparse.ArgumentParser()

    # Version of OpenCV
    parser.add_argument('-v', '--version', choices=['310', '320'])

    # How process output video frame
    parser.add_argument('-i', '--image', choices=['save', 'show', 'save_and_show', 'none'])

    # Input video stream
    parser.add_argument('-s', '--stream', choices=['cam', 'video'])

    # Video stream parameter
    parser.add_argument('-p', '--parameter')

    # Net checking
    parser.add_argument('-n', '--net', choices=['on', 'off'])

    # Wait for camera calibration
    parser.add_argument('-c', '--calibration', choices=['on', 'off'])

    # How to choose object - by bounding box or by coordinates
    parser.add_argument('-b', '--bbox', choices=['coord', 'roi', 'net'])

    # How present debug info - in separate window or in console
    parser.add_argument('-d', '--debug', choices=['gui', 'console', 'off'])

    return parser
