import time
import serial

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial('/dev/ttyS0')

ser.open()

print(ser.isOpen(), ser.port)

print('Enter your commands below.\r\nInsert "exit" to leave the application.')

while 1:
    # send the character to the device
    # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
    ser.write('baka baka mazafaka' + '\r\n')
    out = ''
    # let's wait one second before reading output (let's give device time to answer)
    time.sleep(1)
    while ser.inWaiting() > 0:
        out += ser.read(1)

    if out != '':
        print(">>" + out)
