import cv2


class Track:
    # Setup tracker depends on version
    def setup(self, version):
        # Choose version
        if version == '310':
            # TLD tracker for OpenCV 3.1.0 and earlier
            self.obj = cv2.Tracker_create('TLD')
        elif version == '320':
            # TLD tracker for OpenCV 3.2.0 and later
            self.obj = cv2.TrackerTLD_create()
        else:
            print('False arguments.version')

    # Set tracked object
    def set_obj(self, frame, in_bbox, bbox_choice):
        # By coordinates or from net detection
        if (bbox_choice == 'coord') or (bbox_choice == 'net'):
            # Define an initial bounding box
            bbox = in_bbox

        # By person
        elif bbox_choice == 'roi':
            # Select Region Of Interest
            bbox = cv2.selectROI(frame, False)
            print("Object is selected")

        else:
            print("False arguments.bbox")

        # Initialize tracker with frame and bbox
        status = self.obj.init(frame, bbox)
        if not status:
            status = 'track.set_obj NOT OK'
            print(status)

    # Update object's bbox
    def update(self, frame):
        status, self.bbox = self.obj.update(frame)
        if not status:
            print('track.update NOT OK')
        return status
