import time

import cam
import net
import track


class Hunter:
    # Time to call net
    time_old = 0
    time_new = 0

    def __init__(self, net_check):
        if net_check == 'on':
            self.net_check = True
        elif net_check == 'off':
            self.net_check = False
        else:
            print('False net check')

        self.cam = cam.Cam()
        self.track = track.Track()
        if self.net_check:
            self.net = net.Net()

    def setup(self, stream, version, parameter=None):
        # Camera setup
        self.cam.setup(stream, parameter)

        # Setup tracker type and version
        self.track.setup(version)

        # Load net
        if self.net_check:
            self.net.setup()

    # Check bbox from tracker to bbox from classifier
    def validate(self):
        threshold = 20

        self.net.detect(self.cam.frame)

        if self.net.bbox is None:
            print('Object not found')

        if abs(self.track.bbox[0] - self.net.bbox[0]) > threshold or \
                        abs(self.track.bbox[1] - self.net.bbox[1]) > threshold or \
                        abs(self.track.bbox[2] - self.net.bbox[2]) > threshold or \
                        abs(self.track.bbox[3] - self.net.bbox[3]) > threshold:
            print("Bad tracker")
        else:
            print("Good tracker")

    def calibrate(self):
        self.cam.calibrate()

    def set_obj(self, bbox_choice):
        bbox = (50, 100, 200, 250)

        # Net detection
        if self.net_check:
            flag = True
            while flag:
                self.cam.read_frame()
                flag = self.net.detect(self.cam.frame)
                if not flag:
                    bbox = self.net.bbox
        # Without net calibrate input frame
        else:
            flag = True
            while flag:
                self.cam.read_frame()
                self.cam.get_img('Calibrate', 'show')
                k = cam.cv2.waitKey(500) & 0xff
                if k == 27:
                    flag = 0

        # Set object
        self.track.set_obj(self.cam.frame, bbox, bbox_choice)

    # Update bbox and call NN to verify object
    def update(self):
        # Read next video frame
        self.cam.read_frame()

        # Update tracker
        track_status = self.track.update(self.cam.frame)

        # Show info
        self.cam.get_info('gui')

        # Net checking
        if self.net_check:
            self.time_new = time.time()
            time_dif = self.time_new - self.time_old
            if time_dif > 10:
                print('Reset timer')
                self.net.detect(self.cam.frame)
                self.time_old = self.time_new

                # Reset object
                if not track_status:
                    self.track.set_obj(self.cam.frame, self.net.bbox, 'net')

                # Draw bounding box
                self.cam.draw_bbox('green', self.net.bbox)

                # Present result
                self.cam.get_img('Net', 'show')

        # Draw bounding box and track line
        self.cam.draw_bbox_and_line('red', self.track.bbox)
        # Present result
        self.cam.get_img('Tracking', 'show')

        # Exit if ESC pressed
        flag = self.cam.exit_button()
        return flag
