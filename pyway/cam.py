import time
from collections import deque

import cv2
import numpy as np
from numba import jit


class Cam:
    # Simple font
    _font = cv2.FONT_HERSHEY_SIMPLEX

    # Store how object moves
    _pts = deque(maxlen=64)

    # Calculate FPS
    start = time.time()

    # Wait for camera calibration
    # Interrupt by ESC key
    def calibrate(self):
        flag = True
        while flag:
            self.read_frame()
            self.calc_fps()
            self.get_img('Calibrate', 'show')
            # Exit if ESC pressed
            flag = self.exit_button()

    # Wrapper
    def draw_bbox_and_line(self, color, bbox):
        # Draw bounding box
        _p1, _p2 = self.draw_bbox(color, bbox)

        # Draw line
        #self.draw_line(_p1, _p2)

    # Draw bbox in 3 possible colors
    def draw_bbox(self, color, bbox):
        # Get corner values
        _p1 = (int(bbox[0]), int(bbox[1]))
        _p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))

        # Choose color
        if color == 'red':
            color = (0, 0, 255)
        elif color == 'green':
            color = (0, 255, 0)
        elif color == 'blue':
            color = (255, 0, 0)
        else:
            print('Wrong color')

        # Draw rectangle
        cv2.rectangle(self.frame, _p1, _p2, color, 2)

        return _p1, _p2

    # Draw track line
    def draw_line(self, _p1, _p2):
        # Loop over the set of tracked points
        for i in range(1, len(self._pts)):
            # If either of the tracked points are None,
            # ignore them
            if self._pts[i - 1] is None or self._pts[i] is None:
                continue
            # Otherwise, compute the thickness of the line and
            # draw the connecting lines
            thickness = int(np.sqrt(64 / float(i + 1)) * 2.5)
            cv2.line(self.frame, self._pts[i - 1], self._pts[i], (255, 0, 0), thickness)
        # Update the points queue
        self._pts.appendleft((int((_p1[0] + _p2[0]) / 2),
                              int((_p1[1] + _p2[1]) / 2)))

    # Show debug info
    def get_info(self, info):
        # Present in separate window
        if info == 'gui':
            # Create black background
            _blank_image = np.zeros((250, 250, 3), np.uint8)

            # Add text
            # Processed FPS
            cv2.putText(_blank_image, 'FPS: ' + self.calc_fps(), (10, 30), self._font, 1, (255, 255, 255), 1,
                        cv2.LINE_AA)
            # Show info
            cv2.imshow('Info', _blank_image)

        # Output at console
        elif info == 'console':
            # Processed FPS
            print('FPS: ' + self.calc_fps())

        # Do not present
        elif info == 'off':
            pass

        else:
            print('False arguments.debug')

    # Output frame
    def get_img(self, name, image):
        # Choosing what to do with image
        # Save result
        if image == 'save':
            save = 'test/test.' + str(iter) + '.jpg'
            cv2.imwrite(save, self.frame)

        # Display result
        elif image == 'show':
            cv2.imshow(name, self.frame)

        # Display and save result
        elif image == 'save_and_show':
            # Save result
            save = 'test/test.' + str(iter) + '.jpg'
            cv2.imwrite(save, self.frame)
            # Display result
            cv2.imshow(name, self.frame)

        elif image == 'none':
            pass

        else:
            print('False arguments.image')

    # Open camera and setup tracker type
    def setup(self, stream, parameter=None):
        if stream == 'cam':
            self.video = cv2.VideoCapture(0)
        elif stream == 'video':
            self.video = cv2.VideoCapture(parameter)
        else:
            print('False arguments.stream')
        # If video not opened
        if not self.video.isOpened():
            print('cam.setup NOT OK')

    # Read frame and return it
    def read_frame(self):
        status, self.frame = self.video.read()
        if not status:
            status = 'cam.read_frame NOT OK'
            print(status)

    # Remember new point and return FPS
    @jit
    def calc_fps(self):
        end = time.time()
        seconds = end - self.start
        self.start = end
        return str(int(1 / seconds))

    # Return 0 if ESC button is pressed, 1 - otherwise
    def exit_button(self):
        k = cv2.waitKey(1) & 0xff
        if k == 27:
            return 0
        return 1
