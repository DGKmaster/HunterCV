# xml parser for imagenet
from lxml import etree
import os

def parse(link):
    tree = etree.parse(link)
    folder = tree.findtext('folder')
    filename = tree.findtext('filename')
    for item in tree.iterfind('size'):
        width = item.findtext('width')
        height = item.findtext('height')

    for item in tree.iterfind('object/bndbox'):
        x_min = item.findtext('xmin')
        y_min = item.findtext('ymin')
        x_max = item.findtext('xmax')
        y_max = item.findtext('ymax')

    info = dict(folder=folder, filename=filename, width=width, height=height, x_min=x_min, y_min=y_min,
                x_max=x_max, y_max=y_max)

    return info

def parse_files(link):

    names = os.listdir(link)
    files = []
    for i in names:
        files.append(parse(link + '/' +i))
    return files




