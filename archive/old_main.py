import cv2
import sys
import time
from collections import deque
import numpy as np
import argparse
import net


# Parser for input arguments
def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', choices=['310', '320'])
    parser.add_argument('-i', '--image', choices=['save', 'show', 'save_and_show', 'none'])
    parser.add_argument('-c', '--calibration', choices=['on', 'off'])
    parser.add_argument('-b', '--bbox', choices=['coord', 'roi'])
    return parser

parser = create_parser()
namespace = parser.parse_args(sys.argv[1:])

# Choosing version
if namespace.version == '310':
    # Set up tracker for OpenCV 3.1.0
    # MIL, BOOSTING, KCF, TLD, MEDIANFLOW, GOTURN (not working)
    track_obj = cv2.Tracker_create("TLD")
elif namespace.version == '320':
    # Set up TLD tracker for OpenCV 3.2.0
    track_obj = cv2.TrackerTLD_create()
else:
    print("False version")
    exit(1)

# Read video
video = cv2.VideoCapture(0)

# Exit if video not opened.
if not video.isOpened():
    print("Cannot open camera")
    sys.exit()

# Read first frame.
ok, frame = video.read()

if not ok:
    print('Cannot read video')
    sys.exit()

print("Push Escape when you'll be ready")

# To measure FPS
seconds = 1

# Add Simple font
font = cv2.FONT_HERSHEY_SIMPLEX

if namespace.calibration == 'on':
    # Wait for camera calibration
    while True:
        # Obtain start cycle time
        start = time.time()

        # Add text
        # cv2.putText(frame, 'OpenCV', (10, 50), font, 2, (255, 255, 255), 2, cv2.LINE_AA)
        cv2.putText(frame, str(int(1/seconds)), (10, 110), font, 2, (255, 255, 255), 2, cv2.LINE_AA)

        # To draw a rectangle, you need top-left corner and bottom-right corner of rectangle
        # cv2.rectangle(frame, (0, 10), (40, 40), (0, 255, 0), 2)
        cv2.imshow("Tracking", frame)

        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27:
            break

        ok, frame = video.read()
        if not ok:
            print('Cannot read image from camera')
            sys.exit()
        # Obtain end cycle time
        end = time.time()
        # Compute result
        seconds = end - start
elif namespace.calibration == 'off':
    pass
else:
    print("False calibration status  choice")
    exit(1)

# Choosing object bounding box
if namespace.bbox == 'coord':
    # Define an initial bounding box
    bbox = (287, 23, 86, 320)
elif namespace.bbox == 'roi':
    # Select Region Of Interest
    bbox = cv2.selectROI(frame, False)
    print("Object selected")
else:
    print("False bbox choice")
    exit(1)

# Initialize tracker with first frame and bounding box
ok = track_obj.init(frame, bbox)
bbox = net.predict_and_verify(bbox, frame)

# Start timer
time_old = time.time()

pts = deque(maxlen=64)

while True:
    start = time.time()
    # Read a new frame
    ok, frame = video.read()
    if not ok:
        break

    # Update tracker
    ok, bbox = track_obj.update(frame)

    # Net checking
    time_new = time.time()

    if time_new - time_old > 10:
        print("Reset timer")
        bbox = net.predict_and_verify(bbox, frame)
        time_old = time_new

    # Draw bounding box
    if ok:
        p1 = (int(bbox[0]), int(bbox[1]))
        p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
        cv2.rectangle(frame, p1, p2, (0, 0, 255), 2)

        # Update the points queue
        pts.appendleft((int((p1[0] + p2[0]) / 2),
                        int((p1[1] + p2[1]) / 2)))
        # Loop over the set of tracked points
        for i in range(1, len(pts)):
            # If either of the tracked points are None,
            # ignore them
            if pts[i - 1] is None or pts[i] is None:
                continue

            # Otherwise, compute the thickness of the line and
            # draw the connecting lines
            thickness = int(np.sqrt(64 / float(i + 1)) * 2.5)
            cv2.line(frame, pts[i - 1], pts[i], (255, 0, 0), thickness)

    cv2.putText(frame, str(int(1 / seconds)), (10, 110), font, 2, (255, 255, 255), 2, cv2.LINE_AA)

    # Choosing what to do with images
    if namespace.image == 'save':
        # Save result
        save = 'test/test.' + str(iter) + '.jpg'
        cv2.imwrite(save, frame)
    if namespace.image == 'show':
        # Display result
        cv2.imshow("Tracking", frame)
    elif namespace.image == 'save_and_show':
        # Save result
        save = 'test/test.' + str(iter) + '.jpg'
        cv2.imwrite(save, frame)
        # Display result
        cv2.imshow("Tracking", frame)
    elif namespace.image == 'none':
        pass
    else:
        print("False image choice")
        exit(1)

    # Exit if ESC pressed
    k = cv2.waitKey(1) & 0xff
    if k == 27:
        break

    end = time.time()
    seconds = end - start
