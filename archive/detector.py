import cv2
import net

for i in range(1, 8):
    img = cv2.imread(f'{i}.png')

    bbox = net.detect(img)

    try:
        p1 = (int(bbox[0]), int(bbox[1]))
    except TypeError:
        print('Object not found')
        continue

    p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
    cv2.rectangle(img, p1, p2, (0, 255, 0), 2)

    cv2.imshow(f'{i}1', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    cv2.imwrite(f'{i}1.png', img)