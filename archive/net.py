# Импорт библиотек и модулей
from keras import backend as K
K.set_image_dim_ordering('tf')
import numpy as np
import csv
import matplotlib as plt
from matplotlib import image
import lxml
from keras.models import Model
from keras.layers import Input, Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from xml_parser import parse_files as pf
from img_loader import load
# random for weights
np.random.seed(123)
import cv2

input_layer = Input(shape = (100, 100, 3), name='input_layer')

conv1 = Convolution2D(filters=32, kernel_size=(3, 3), activation='relu')(input_layer)
max1 = MaxPooling2D(pool_size=(3, 3))(conv1)
drop1 = Dropout(0.5)(max1)

conv2 = Convolution2D(filters=64, kernel_size=(3, 3), activation='relu')(drop1)
max2 = MaxPooling2D(pool_size=(3, 3))(conv2)
drop2 = Dropout(0.5)(max2)

conv3 = Convolution2D(filters=96, kernel_size=(3, 3), activation='relu')(drop2)
max3 = MaxPooling2D(pool_size=(3, 3))(conv3)

flatten1 = Flatten()(max3)

drop3 = Dropout(0.5)(flatten1)
dense1 = Dense(256)(drop3)

drop4 = Dropout(0.5)(flatten1)
dense20 = Dense(256)(drop4)
drop5 = Dropout(0.5)(dense20)
dense2 = Dense(256)(drop5)

drop6 = Dropout(0.5)(flatten1)
dense30 = Dense(256)(drop6)
drop7 = Dropout(0.5)(dense30)
dense3 = Dense(256)(drop7)

drop8 = Dropout(0.5)(flatten1)
dense40 = Dense(256)(drop8)
drop9 = Dropout(0.5)(dense40)
dense4 = Dense(256)(drop9)

drop10 = Dropout(0.5)(flatten1)
dense50 = Dense(256)(drop10)
drop11 = Dropout(0.5)(dense50)
dense5 = Dense(256)(drop11)

# classificator
drop12 = Dropout(0.5)(dense1)
output_c = Dense(2, activation='softmax', name='output_c')(drop12)

# bounding box
# x1
drop13 = Dropout(0.5)(dense2)
output_x0 = Dense(1, activation='linear', name='output_x0')(drop13)
# y1
drop14 = Dropout(0.5)(dense3)
output_y0 = Dense(1, activation='linear', name='output_y0')(drop14)
# width
drop15 = Dropout(0.5)(dense4)
output_x1 = Dense(1, activation='linear', name='output_x1')(drop15)
# height
drop16 = Dropout(0.5)(dense5)
output_y1 = Dense(1, activation='linear', name='output_y1')(drop16)

model = Model(inputs=input_layer, outputs=[output_c, output_x0, output_y0, output_x1, output_y1])

model.compile(optimizer='adam', loss={'output_c': 'binary_crossentropy', 'output_x0': 'mean_squared_error',
                                      'output_y0': 'mean_squared_error', 'output_x1': 'mean_squared_error',
                                      'output_y1': 'mean_squared_error'})

model.load_weights('model-1.5')
img = load('kotest')
data = []
data.append(img/255)
data = np.array(data)
prediction = model.predict(data, 1)
x0 = (int(prediction[1]*255))
y0 = (int(prediction[2]*255))
x1 = (int(prediction[3]*255))
y1 = (int(prediction[4]*255))

print(prediction[0])
print(x0, y0, x1, y1)

cv2.rectangle(img, (x0, y0), (x1, y1), (0, 255, 0), 2)
cv2.imshow("Tracking", img)

cv2.waitKey(0)
cv2.destroyAllWindows()