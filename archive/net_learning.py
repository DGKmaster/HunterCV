# Импорт библиотек и модулей
from keras import backend as K
K.set_image_dim_ordering('tf')
import numpy as np
import csv
import matplotlib as plt
from matplotlib import image
import lxml
from keras.models import Model
from keras.layers import Input, Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils
from xml_parser import parse_files as pf
from img_loader import load, load1
# random for weights
np.random.seed(123)
import cv2
import os

link = 'data/Annotation/n04409515'



data = pf(link)
print(data[2]['filename'])
X_data = []
Y1_data = []
Y2_data = []
Y3_data = []
Y4_data = []
Y5_data = []
for i in range(len(data)):
    x = load(data[i]['filename'])/255
    X_data.append(x)
    y1 = round(int(data[i]['x_min']) * 100 / (int(data[i]['width'])))/255
    Y1_data.append(y1)
    y2 = round(int(data[i]['y_min']) * 100 / (int(data[i]['height'])))/255
    Y2_data.append(y2)
    y3 = round(int(data[i]['x_max']) * 100 / (int(data[i]['width'])))/255
    Y3_data.append(y3)
    y4 = round(int(data[i]['y_max']) * 100 / (int(data[i]['height'])))/255
    Y4_data.append(y4)
    Y5_data.append(0)
no_data = os.listdir('data/n00017222')

for i in range(len(no_data)):
    x = load1('n00017222/' + no_data[i]) / 255
    X_data.append(x)
    y1 = 0
    Y1_data.append(y1)
    y2 = 0
    Y2_data.append(y2)
    y3 = 0
    Y3_data.append(y3)
    y4 = 0
    Y4_data.append(y4)
    Y5_data.append(1)


X_data = np.array(X_data)
Y1_data = np.array(Y1_data)
Y2_data = np.array(Y2_data)
Y3_data = np.array(Y3_data)
Y4_data = np.array(Y4_data)
Y5_data = np.array(Y5_data)
Y5_data = np_utils.to_categorical(Y5_data, 2)

vgg

