# HunterCV

НИР в сфере обработки изображения, состоящая в проверке гипотезы улучшения
качества работы одного из алгоритмов слежения путём добавления априорной
информации об объекте слежения из обученной ИНС.

---

## Проект состоит из следующих частей

**vscpp** - Проект Windows MSVS2017 OpenCV С++ для
быстрого тестирования решений.

**CLcpp** - C++ Linux проект в CodeLite для переноса кода OpenCV
с Python на C++.

**stm32** - STM32 проект системы управления приводами робота.

**pyway** - Python Linux проект прототипа работы алгоритма слежения совместно с ИНС.

**archive** - Файлы, которые больше не используются, но могут быть полезны в будущем.

## Назначение проектов в C++

**Main** - главная программа, которая подгружает статическую библиотеку Hunter и проводит ввод всех параметров работы

**Hunter** - статическая библиотека, содержащая класс с логикой взаимодействия Track и Net

**Track** - статическая библиотека, содержащая класс работы с OpenCV tracking module

**Net** - статическая библиотека, содержащая класс работы с OpenCV DNN module

## Параметры main.py для запуска Python проекта

```bash
-v 320 -i show -c on -b roi -d gui
```

---

## Алгоритм настройки окружения

### Сборка OpenCV

1. [Выполнить все инструкции за исключением создания virtualenv](http://www.learnopencv.com/install-opencv3-on-ubuntu/)
2. Установить по инструкции заиенив следующую команду

```bash
sudo make install -> sudo checkinstall
```

### Установка darkflow

```bash
# Скачать репозиторий darkflow
git clone https://github.com/thtrieu/darkflow.git

# Установить зависимости
sudo -H pip3 install tensorflow tensorflow-gpu cython

# Перейти в репозиторий
cd darkflow

# Установить
sudo -H pip3 install -e .
```

### Установка IDE для Python - PyCharm

1. [Скачать Community Edition](https://www.jetbrains.com/pycharm/download/#section=linux)
2. Распаковать и переместить в папку установки
3. Открыть корневой каталог в терминале

```bash
cd bin
chmod +x
./pycharm.sh
```

### Установка CUDA Toolkit

1. [Скачать нужную версию](https://developer.nvidia.com/cuda-downloads)

### Установка darknet (optional)

[Официальный сайт](https://pjreddie.com/darknet/yolo/)

```bash
# Скачать репозиторий
git clone https://github.com/pjreddie/darknet

# Перейти в корень
cd darknet

# Установить
make
```

### Удаление OpenCV

1. Зайти в корневой каталог
2. Выполнить команду

```bash
dpkg -r build
```

---

## References

* [ТХ Jetson TK1](https://elinux.org/Jetson_TK1)
* [UART Connection Example](https://github.com/cheydrick/Canonical-Arduino-Read/blob/master/canonicalarduinoread.c)
* [Odometry](http://robocraft.ru/blog/technology/736.html)
* [Настройка симулятора Gazebo для программы на Python с СТЗ](https://geektimes.ru/company/makeitlab/blog/288506/)
* [Форматирование всего в строку C++](http://my-it-notes.com/2012/08/how-to-convert-number-to-string-c/)
* [Building TensorFlow for Jetson TK1](http://cudamusing.blogspot.ru/2015/11/building-tensorflow-for-jetson-tk1.html)

## Справочная информация

Путь до компилятора MSVS 2017:

```cmd
C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.10.25017\bin\HostX64\x64
```

Подключение камеры
Чтобы появилось стандартное V4L устройство /dev/video0, нужно всего-лишь выполнить:

```bash
sudo modprobe bcm2835-v4l2
```
